/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microservicesector.proxies;

import java.net.URISyntaxException;
import org.agrosfer.pdm.microservicesector.beans.ReferenceBean;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 * @author g3a
 */
//@RibbonClient("microservice-codification")
@FeignClient(name = "microservice-codification", url = "localhost:9040")
public interface MicroserviceCodificationProxy {
    
    @PostMapping("/references")
    ResponseEntity<?> createReference(@RequestBody ReferenceBean reference) throws URISyntaxException;
}
