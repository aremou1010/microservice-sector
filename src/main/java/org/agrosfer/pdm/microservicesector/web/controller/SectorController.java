/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microservicesector.web.controller;

import java.net.ConnectException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Objects;
import java.util.Optional;
import org.agrosfer.pdm.microservicesector.beans.ReferenceBean;
import org.agrosfer.pdm.microservicesector.configs.ApplicationPropertiesConfig;
import org.agrosfer.pdm.microservicesector.dao.ProductDAO;
import org.agrosfer.pdm.microservicesector.dao.SectorDAO;
import org.agrosfer.pdm.microservicesector.model.Product;
import org.agrosfer.pdm.microservicesector.model.Sector;
import org.agrosfer.pdm.microservicesector.proxies.MicroserviceCodificationProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author g3a
 */
@RestController
public class SectorController {

    @Autowired
    SectorDAO sectorDAO;

    @Autowired
    ProductDAO productDAO;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    ApplicationPropertiesConfig apc;

    @PostMapping("/sectors")
    public ResponseEntity<?> createSector(@RequestBody Sector sector) {

        sector.setDesignation(sector.getDesignation().toLowerCase());
        sector.setCreateAt(Timestamp.from(Instant.now()));
        sector.setUpdateAt(Timestamp.from(Instant.now()));
        sector.setIsDelete(Boolean.FALSE);

        String ref = "";
        ref = getReference(Sector.class.getSimpleName());

        if (ref == "") {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Sector cannot be created. Go to check microservice-codification");
        }

        if (sectorDAO.findByDesignationAndIsDeleteFalse(sector.getDesignation().toLowerCase()).isPresent()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("SECTOR EXIST");
        }

        sector.setReference(ref);
        Sector newSector = sectorDAO.save(sector);

        if (newSector != null) {
            return ResponseEntity.accepted()
                    .location(URI.create(apc.getUrl() + "/sectors/" + newSector.getReference()))
                    .body(newSector);
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Sector cannot be created");
    }

//    @PutMapping("/sectors")
    @PostMapping("/sectors/update")
    public ResponseEntity<?> updateSector(@RequestBody Sector sector) {

        Optional<Sector> oneSector = sectorDAO.findByReferenceAndIsDeleteFalse(sector.getReference());

        if (oneSector.isPresent()) {

            if (!sector.getDesignation().toLowerCase().equals(oneSector.get().getDesignation()) && sectorDAO.findByDesignationAndIsDeleteFalse(sector.getDesignation().toLowerCase()).isPresent()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("SECTOR EXIST");
            }
            oneSector.get().setDesignation(sector.getDesignation().toLowerCase());
            oneSector.get().setNature(sector.getNature());
            oneSector.get().setFamily(sector.getFamily());
            oneSector.get().setUpdateAt(Timestamp.from(Instant.now()));
        }

        Sector updatedSector = sectorDAO.saveAndFlush(oneSector.get());

        if (updatedSector != null) {
            return ResponseEntity.accepted()
                    .location(URI.create(apc.getUrl() + "/sectors/" + updatedSector.getReference()))
                    .body(updatedSector);
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Sector cannot be updated");

    }

    @GetMapping("/sectors")
    public ResponseEntity<?> getAllSectors() {
        return ResponseEntity.ok(sectorDAO.findAllByIsDeleteFalse());
    }

    @GetMapping("/sectors/id/{id}")
    public ResponseEntity<?> getOneSectorById(@PathVariable("id") Long id) {
        Optional<Sector> oneSector;
        oneSector = sectorDAO.findByIdAndIsDeleteFalse(id);

        if (oneSector.isPresent()) {
            return ResponseEntity.of(oneSector);
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body("This sector does not exist");
    }

    @GetMapping("/sectors/{ref}")
    public ResponseEntity<?> getOneSectorByReference(@PathVariable("ref") String ref) {
        Optional<Sector> oneSector;
        oneSector = sectorDAO.findByReferenceAndIsDeleteFalse(ref);

        if (oneSector.isPresent()) {
            return ResponseEntity.of(oneSector);
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body("This sector does not exist");

    }

    //@DeleteMapping("/sectors/id/{id}")  //Revoir la suppression en supprimant les produits qui en découlent
    public ResponseEntity<?> deleteOneSector(@PathVariable("id") Long id) {
        Optional<Sector> oneSector;
        oneSector = sectorDAO.findByIdAndIsDeleteFalse(id);

        if (oneSector.isPresent()) {

            oneSector.get().setIsDelete(Boolean.TRUE);
            oneSector.get().setDeleteAt(Timestamp.from(Instant.now()));
            Sector sectorUpdated = sectorDAO.saveAndFlush(oneSector.get());

            if (sectorUpdated != null) {
                return ResponseEntity.ok()
                        .body("This sector is successfully removed");
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("This sector cannot be removed");
            }

        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body("This sector does not exist");
    }

    //@DeleteMapping("/sectors/{ref}")  //Revoir la suppression en supprimant les produits qui en découlent
    public ResponseEntity<?> deleteOneSectorByReference(@PathVariable("ref") String ref) {
        Optional<Sector> oneSector;
        oneSector = sectorDAO.findByReferenceAndIsDeleteFalse(ref);

        if (oneSector.isPresent()) {

            oneSector.get().setIsDelete(Boolean.TRUE);
            oneSector.get().setDeleteAt(Timestamp.from(Instant.now()));
            Sector sectorUpdated = sectorDAO.saveAndFlush(oneSector.get());

            if (sectorUpdated != null) {
                return ResponseEntity.ok()
                        .body("This sector is successfully removed");
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("This sector cannot be removed");
            }

        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body("This sector does not exist");
    }

    //=========================== PRODUCTS ===========================//
    //@PostMapping("/sectors/{sectorRef}/products")
    public ResponseEntity<?> createAProduct(@PathVariable("sectorRef") String sectorRef, @RequestBody Product product) {
        product.setCreateAt(Timestamp.from(Instant.now()));
        product.setUpdateAt(Timestamp.from(Instant.now()));
        product.setIsDelete(Boolean.FALSE);

        String ref = "";
        ref = getReference(Product.class.getSimpleName());

        if (ref == "") {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Product cannot be created. Go to check microservice-codification");
        }

        product.setReference(ref);
        Product newProduct = productDAO.save(product);

        if (newProduct != null) {
            return ResponseEntity.accepted()
                    .location(URI.create(apc.getUrl() + "/sectors/" + sectorRef + "/products/" + newProduct.getId()))
                    .body(newProduct);
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Product cannot be created");
    }

    @PostMapping("/products")
    public ResponseEntity<?> createProduct(@RequestBody Product product) {

//        Optional<Sector> oneSector = sectorDAO.findByReferenceAndIsDeleteFalse(product.getParentReference());
//        Optional<Product> oneProduct = productDAO.findByReferenceAndIsDeleteFalse(product.getParentReference());
//        if (oneSector.isPresent()) {
//            product.setProductLevel(1);
//        }
//        if (oneProduct.isPresent()) {
//            product.setProductLevel(oneProduct.get().getProductLevel()+1);
//        }
        product.setDesignation(product.getDesignation().toLowerCase());
        product.setCreateAt(Timestamp.from(Instant.now()));
        product.setUpdateAt(Timestamp.from(Instant.now()));
        product.setIsDelete(Boolean.FALSE);

        String ref = "";
        ref = getReference(Product.class.getSimpleName());

        if (productDAO.findByDesignationAndIsDeleteFalse(product.getDesignation().toLowerCase()).isPresent()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("PRODUCT EXIST");
        }

        if (ref == "") {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Product cannot be created. Go to check microservice-codification");
        }

        product.setReference(ref);
        Product newProduct = productDAO.save(product);

        if (newProduct != null) {
            return ResponseEntity.accepted()
                    .location(URI.create(apc.getUrl() + "/products/" + newProduct.getReference()))
                    .body(newProduct);
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Product cannot be created");

    }

    @GetMapping("/products")
    public ResponseEntity<?> getAllProducts() {

        return ResponseEntity.ok()
                .body(productDAO.findAllByIsDeleteFalse());

    }

    @GetMapping("/products/{ref}")
    public ResponseEntity<?> getOneProductByReference(@PathVariable("ref") String ref) {
        Optional<Product> oneProduct;
        oneProduct = productDAO.findByReferenceAndIsDeleteFalse(ref);

        if (oneProduct.isPresent()) {
            return ResponseEntity.of(oneProduct);
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body("This product does not exist");

    }

//    @PutMapping("/products")
    @PostMapping("/products/update")
    public ResponseEntity<?> updateProduct(@RequestBody Product product) {

        Optional<Product> oneProduct = productDAO.findByReferenceAndIsDeleteFalse(product.getReference());

        if (oneProduct.isPresent()) {

            if (!product.getDesignation().toLowerCase().equals(oneProduct.get().getDesignation()) && productDAO.findByDesignationAndIsDeleteFalse(product.getDesignation().toLowerCase()).isPresent()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("PRODUCT EXIST");
            }
            oneProduct.get().setDesignation(product.getDesignation().toLowerCase());
            oneProduct.get().setProductLevel(product.getProductLevel());
            oneProduct.get().setParentReference(product.getParentReference());
            oneProduct.get().setUpdateAt(Timestamp.from(Instant.now()));
        }

        Product updatedProduct = productDAO.saveAndFlush(oneProduct.get());

        if (updatedProduct != null) {
            return ResponseEntity.accepted()
                    .location(URI.create(apc.getUrl() + "/products/" + updatedProduct.getReference()))
                    .body(updatedProduct);
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Product cannot be updated");

    }

    @GetMapping("/sectors/{sectorRef}/level/{productsLevel}/products")
    public ResponseEntity<?> getProductsByLevel(@PathVariable("sectorRef") String sectorRef, @PathVariable("productsLevel") int productsLevel) {
        return ResponseEntity.ok(productDAO.findAllBySectorReferenceAndProductLevelAndIsDeleteFalse(sectorRef, productsLevel));
    }

//    @GetMapping("/products/ref")
//    public ResponseEntity<?> test() throws URISyntaxException {
//        ReferenceBean rb = new ReferenceBean();
//        rb.setEntity(Product.class.getSimpleName());
//
//        //Method1
//        HttpEntity<?> request = new HttpEntity<>(rb);
//        ResponseEntity<ReferenceBean> re;
//        try {
//            re = restTemplate.exchange(URI.create(apc.getMsCodificationCreateUrl()), HttpMethod.POST, request, ReferenceBean.class);
//            return ResponseEntity.ok(re.getBody());
//
//        } catch (RestClientException e) {
//            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(e.getCause().getMessage()+" to microservice-codification");
//        }
//
//        //Method2
//        //ReferenceBean newR = restTemplate.postForObject(URI.create(apc.getMsCodificationCreateUrl()), rb, ReferenceBean.class);
//        //return ResponseEntity.ok(newR);
//    }
    public String getReference(String entityName) {
        ReferenceBean rb = new ReferenceBean();
        rb.setEntity(entityName);

        //Method1
        HttpEntity<?> request = new HttpEntity<>(rb);
        ResponseEntity<ReferenceBean> re;
        try {
            re = restTemplate.exchange(URI.create(apc.getMsCodificationCreateUrl()), HttpMethod.POST, request, ReferenceBean.class);
            return re.getBody().getCode();

        } catch (RestClientException e) {
            return ("");
        }

        //Method2
        //ReferenceBean newR = restTemplate.postForObject(URI.create(apc.getMsCodificationCreateUrl()), rb, ReferenceBean.class);
        //return ResponseEntity.ok(newR);
    }
}
