/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microservicesector.model;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Data;

/**
 *
 * @author g3a
 */
@Data
@Entity
public class Product implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    private String reference;
    
    private String designation;
    
    private String fullDesignation;
    
    private int productLevel;
    
    private String parentReference;
    
    private String sectorReference;
    
    private Timestamp createAt;
    
    private Timestamp updateAt;
    
    private Timestamp deleteAt;
    
    private Boolean isDelete;

    public Product() {
    }

    public Product(Long id, String reference, String designation, String fullDesignation, int productLevel, String parentReference, String sectorReference, Timestamp createAt, Timestamp updateAt, Timestamp deleteAt, Boolean isDelete) {
        this.id = id;
        this.reference = reference;
        this.designation = designation;
        this.fullDesignation = fullDesignation;
        this.productLevel = productLevel;
        this.parentReference = parentReference;
        this.sectorReference = sectorReference;
        this.createAt = createAt;
        this.updateAt = updateAt;
        this.deleteAt = deleteAt;
        this.isDelete = isDelete;
    }
    
}
