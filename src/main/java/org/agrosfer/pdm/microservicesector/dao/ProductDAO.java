/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microservicesector.dao;

import java.util.List;
import java.util.Optional;
import org.agrosfer.pdm.microservicesector.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author g3a
 */
public interface ProductDAO extends JpaRepository<Product, Long> {
    
    Optional<Product> findByReferenceAndIsDeleteFalse(String ref);
    
    List<Product> findAllBySectorReferenceAndProductLevelAndIsDeleteFalse(String ref, int level);
    
    List<Product> findAllByIsDeleteFalse();
    
    Optional<Product> findByDesignationAndIsDeleteFalse(String designation);
    
}
