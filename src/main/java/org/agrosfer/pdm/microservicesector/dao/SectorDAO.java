/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microservicesector.dao;

import java.util.List;
import java.util.Optional;
import org.agrosfer.pdm.microservicesector.model.Sector;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author g3a
 */
public interface SectorDAO extends JpaRepository<Sector, Long> {
    
    List<Sector> findAllByIsDeleteFalse();
    
    Optional<Sector> findByIdAndIsDeleteFalse(Long id);
    
    Optional<Sector> findByReferenceAndIsDeleteFalse(String ref);
    
    Optional<Sector> findByDesignationAndIsDeleteFalse(String designation);
    
}
